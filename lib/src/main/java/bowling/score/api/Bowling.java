package bowling.score.api;

import bowling.score.api.exception.RuleViolation;
import bowling.score.api.model.Frame;
import bowling.score.api.model.Game;
import bowling.score.api.model.Player;

import java.util.List;


public class Bowling {

    // Create player
    public Player createPlayer(String playerName) {
        return new Player(playerName);
    }

    // Create game
    public Game createGame(List<Player> players) {
        return new Game(players);
    }

    // Get points of certain player
    public Integer getPoints(Player player) {
        return player.getTotalScore();
    }

    // Get table of points for certain player
    public List<Frame> getPointsTable(Player player) {
        return player.getFrames();
    }

    // Add points to the player
    public void addPoints(Game game, Player player, List<Integer> playerThrows) throws Exception {

        for (Integer points : playerThrows) {
            if (points < 0 || points > 10) {
                throw RuleViolation.incorrectInput();
            }
        }

        if (isInvalidSumOfPoints(game, playerThrows)) {
            throw RuleViolation.tooManyPoints();
        }

        if (playerThrows.size() > 3) {
            throw RuleViolation.tooManyShots();
        }

        if (!isGameOver(player)) {
            if (!areShotsDoneInCurrentFrame(game, player)) {
                Frame frame = new Frame();
                setPoints(game, player, playerThrows, frame);

                countPointsForPreviousStrikesAndSpares(player, frame);

                addFrame(player, frame);

                updateRunningTotalForPreviousFrames(player);

                player.setTotalScore(frame.getRunningTotal());

                if (areShotsDoneForAllPlayersInCurrentFrame(game)) {
                    game.setCurrentFrame(game.getCurrentFrame() + 1);
                }
            } else {
                throw RuleViolation.shotsAlreadyDone();
            }
        } else {
            throw RuleViolation.gameIsOver();
        }
    }

    // Check if the provided points have an incorrect sum
    private boolean isInvalidSumOfPoints(Game game, List<Integer> playerThrows) {
        return (playerThrows.size() == 2 && game.getCurrentFrame() != 10 && playerThrows.get(0) + playerThrows.get(1) > 10)
                || (playerThrows.size() == 3 && playerThrows.get(0) < 10 && playerThrows.get(1) < 10 && playerThrows.get(0) + playerThrows.get(1) > 10)
                || (playerThrows.size() == 3 && playerThrows.get(1) < 10 && playerThrows.get(2) < 10 && playerThrows.get(1) + playerThrows.get(2) > 10);
    }

    // Check if the player has done their shots in current frame
    private boolean areShotsDoneInCurrentFrame(Game game, Player player) {
        return player.getFrames().size() != game.getCurrentFrame() - 1;
    }

    // Check if the game is over
    private boolean isGameOver(Player player) {
        return player.getFrames().size() == 10;
    }

    // Set points for the frame and replace strikes and spares with corresponding symbols
    private void setPoints(Game game, Player player, List<Integer> playerThrows, Frame frame) {
        if (playerThrows.size() == 1 && game.getCurrentFrame() != 10) { // Strike
            if (playerThrows.get(0) == 10) {
                frame.setFirstThrow("X");
                frame.setFrameScore(10);
            } else {
                throw RuleViolation.notEnoughShots();
            }
        } else if (game.getCurrentFrame() == 10) {
            if (playerThrows.size() == 1 || (playerThrows.size() == 2 && playerThrows.get(0) == 10)
                    || (playerThrows.size() == 2 && playerThrows.get(0) + playerThrows.get(1) == 10)) {
                throw RuleViolation.notEnoughShots();
            }
        }

        if (playerThrows.size() == 2) {
            frame.setFirstThrow(playerThrows.get(0));
            if (playerThrows.get(0) + playerThrows.get(1) == 10) frame.setSecondThrow("/");
            else frame.setSecondThrow(playerThrows.get(1));

            frame.setFrameScore(playerThrows.get(0) + playerThrows.get(1));
        }

        if (playerThrows.size() == 3) {
            if (player.getFrames().size() == 9 && (playerThrows.get(0) == 10 || playerThrows.get(0) + playerThrows.get(1) == 10)) {
                if (playerThrows.get(0) == 10) {
                    frame.setFirstThrow("X");
                    frame.setFrameScore(10);
                } else {
                    frame.setFirstThrow(playerThrows.get(0));
                    frame.setFrameScore(playerThrows.get(0));
                }

                if (playerThrows.get(1) == 10) {
                    frame.setSecondThrow("X");
                    frame.setFrameScore(frame.getFrameScore() + 10);
                } else {
                    frame.setSecondThrow(playerThrows.get(1));
                    frame.setFrameScore(frame.getFrameScore() + playerThrows.get(1));
                }

                if (playerThrows.get(2) == 10) {
                    frame.setThirdThrow("X");
                    frame.setFrameScore(frame.getFrameScore() + 10);
                } else {
                    frame.setThirdThrow(playerThrows.get(2));
                    frame.setFrameScore(frame.getFrameScore() + playerThrows.get(2));
                }

                if (playerThrows.get(0) + playerThrows.get(1) == 10) {
                    frame.setSecondThrow("/");
                }

                if (playerThrows.get(1) + playerThrows.get(2) == 10) {
                    frame.setThirdThrow("/");
                }

            } else {
                throw RuleViolation.notPermittedForThreeShots();
            }
        }
    }

    // Count points for previous frames if there were strikes or spares
    private void countPointsForPreviousStrikesAndSpares(Player player, Frame frame) {
        if (player.getFrames().size() > 1) {
            Frame previousFrame = player.getFrames().get(player.getFrames().size() - 1);
            Frame prePreviousFrame = player.getFrames().get(player.getFrames().size() - 2);


            // If previous frame and frame before that were both strikes
            if (previousFrame.getFirstThrow().equals("X") && prePreviousFrame.getFirstThrow().equals("X")) {
                if (frame.getFirstThrow().equals("X")) {
                    prePreviousFrame.setFrameScore(prePreviousFrame.getFrameScore() + previousFrame.getFrameScore() + 10);
                } else {
                    prePreviousFrame.setFrameScore(prePreviousFrame.getFrameScore() + previousFrame.getFrameScore() + Integer.parseInt(frame.getFirstThrow()));
                    previousFrame.setFrameScore(previousFrame.getFrameScore() + frame.getFrameScore());
                }

                if (player.getFrames().size() == 9 && previousFrame.getFirstThrow().equals("X")) {

                    if (frame.getFirstThrow().equals("X"))
                        previousFrame.setFrameScore(previousFrame.getFrameScore() + 10);
                    else {
                        previousFrame.setFrameScore(previousFrame.getFrameScore() + Integer.parseInt(frame.getFirstThrow()));
                    }

                    if (frame.getSecondThrow().equals("X")) {
                        previousFrame.setFrameScore(previousFrame.getFrameScore() + 10);
                    } else if (frame.getSecondThrow().equals("/")) {
                        previousFrame.setFrameScore(previousFrame.getFrameScore() + 10);
                    } else {
                        previousFrame.setFrameScore(previousFrame.getFrameScore() + Integer.parseInt(frame.getSecondThrow()));
                    }
                }
            }

            // If current frame is a spare and previous frame was a strike
            else if (frame.getSecondThrow() != null && frame.getSecondThrow().equals("/") && previousFrame.getFirstThrow().equals("X")) {
                previousFrame.setFrameScore(previousFrame.getFrameScore() + 10);
            }

            // If previous frame was a strike and frame before that was not a strike
            else if (frame.getSecondThrow() != null && previousFrame.getFirstThrow().equals("X") && !prePreviousFrame.getFirstThrow().equals("X")) {
                previousFrame.setFrameScore(previousFrame.getFrameScore() + frame.getFrameScore());
            }

            // If previous frame was a spare
            else if (previousFrame.getSecondThrow() != null && previousFrame.getSecondThrow().equals("/")) {
                if (frame.getFirstThrow().equals("X")) {
                    previousFrame.setFrameScore(previousFrame.getFrameScore() + 10);
                } else {
                    previousFrame.setFrameScore(previousFrame.getFrameScore() + Integer.parseInt(frame.getFirstThrow()));
                }
            }

            // If previous frame was a strike
            else if (previousFrame.getFirstThrow().equals("X")) {
                previousFrame.setFrameScore(frame.getFrameScore());
            }

        }

        if (player.getFrames().size() == 1) {
            Frame previousFrame = player.getFrames().get(player.getFrames().size() - 1);

            // If previous frame was a spare and current frame is a strike
            if (previousFrame.getSecondThrow() != null && previousFrame.getSecondThrow().equals("/")) {
                if (frame.getFirstThrow().equals("X")) {
                    previousFrame.setFrameScore(previousFrame.getFrameScore() + 10);
                } else {
                    previousFrame.setFrameScore(previousFrame.getFrameScore() + Integer.parseInt(frame.getFirstThrow()));
                }
            }

            // If previous frame was a strike and current frame is not a strike
            if (previousFrame.getFirstThrow().equals("X") && !frame.getFirstThrow().equals("X")) {
                previousFrame.setFrameScore(previousFrame.getFrameScore() + frame.getFrameScore());
            }
        }
    }

    // Add a played frame for the player
    private void addFrame(Player player, Frame frame) {
        List<Frame> frames = player.getFrames();
        frames.add(frame);
        player.setFrames(frames);

        player.setTotalScore(frame.getRunningTotal());
    }

    // Update running total for previous frames according to the updates in scores of the frames
    private void updateRunningTotalForPreviousFrames(Player player) {
        for (int i = 0; i < player.getFrames().size(); i++) {
            int runningTotal = 0;
            for (int j = i; j >= 0; j--) {
                runningTotal = runningTotal + player.getFrames().get(j).getFrameScore();
            }
            player.getFrames().get(i).setRunningTotal(runningTotal);
        }
    }

    // Check if all the players have done their shots in the current frame
    private boolean areShotsDoneForAllPlayersInCurrentFrame(Game game) {
        return game.getPlayers().stream().allMatch(p -> p.getFrames().size() == game.getCurrentFrame()) && game.getCurrentFrame() != 10;
    }
}
