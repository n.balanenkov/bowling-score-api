package bowling.score.api.exception;

public class RuleViolation extends RuntimeException {

    public RuleViolation(final String message){
        super(message);
    }

    public static RuleViolation notEnoughShots(){
        String message = "Please provide a full list of shots for that frame.";
        return new RuleViolation(message);
    }

    public static RuleViolation shotsAlreadyDone(){
        String message = "You can't add points more than once during one frame.";
        return new RuleViolation(message);
    }

    public static RuleViolation gameIsOver(){
        String message = "The game is over, you can't add any more points.";
        return new RuleViolation(message);
    }

    public static RuleViolation notPermittedForThreeShots(){
        String message = "You can't do the third shot unless it's 10th frame and you have scored a strike or spare during first two shots.";
        return new RuleViolation(message);
    }

    public static RuleViolation tooManyShots() {
        String message = "You have done too many shots in that frame.";
        return new RuleViolation(message);
    }

    public static RuleViolation tooManyPoints() {
        String message = "The sum of two shots without a strike can not be bigger than 10.";
        return new RuleViolation(message);
    }

    public static Exception incorrectInput() {
        String message = "Amount of points should be positive and not bigger than 10.";
        return new RuleViolation(message);
    }
}

