package bowling.score.api.model;

public class Frame {
    private String firstThrow;
    private String secondThrow;
    private String thirdThrow;
    private Integer frameScore;
    private Integer runningTotal;

    public Frame() {
    }

    public String getFirstThrow() {
        return firstThrow;
    }

    public void setFirstThrow(Integer firstThrow) {
        this.firstThrow = firstThrow.toString();
    }

    public void setFirstThrow(String firstThrow) {
        this.firstThrow = firstThrow;
    }

    public String getSecondThrow() {
        return secondThrow;
    }

    public void setSecondThrow(Integer secondThrow) {
        this.secondThrow = secondThrow.toString();
    }

    public void setSecondThrow(String secondThrow) {
        this.secondThrow = secondThrow;
    }

    public String getThirdThrow() {
        return thirdThrow;
    }

    public void setThirdThrow(Integer thirdThrow){
        this.thirdThrow = thirdThrow.toString();
    }

    public void setThirdThrow(String thirdThrow) {
        this.thirdThrow = thirdThrow;
    }

    public Integer getFrameScore() {
        return frameScore;
    }

    public void setFrameScore(Integer frameScore) {
        this.frameScore = frameScore;
    }

    public Integer getRunningTotal() {
        return runningTotal;
    }

    public void setRunningTotal(Integer runningTotal) {
        this.runningTotal = runningTotal;
    }

    @Override
    public String toString() {
        if (secondThrow == null && thirdThrow == null)
            return "Frame{" +
                    "First throw: " + firstThrow +
                    ", Frame score: " + frameScore +
                    ", Running total: " + runningTotal +
                    '}';

        if (thirdThrow == null)
            return "Frame{" +
                    "First throw: " + firstThrow +
                    ", Second throw: " + secondThrow +
                    ", Frame score: " + frameScore +
                    ", Running total: " + runningTotal +
                    '}';

        return "Frame{" +
                "First throw: " + firstThrow +
                ", Second throw: " + secondThrow +
                ", Third throw: " + thirdThrow +
                ", Frame score: " + frameScore +
                ", Running total: " + runningTotal +
                '}';
    }
}
