package bowling.score.api.model;

import java.util.ArrayList;
import java.util.List;

public class Player {
    private String name;
    private Integer totalScore;
    private List<Frame> frames;

    public Player(String name) {
        this.name = name;
        this.totalScore = 0;
        this.frames = new ArrayList<>();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getTotalScore() {
        return totalScore;
    }

    public void setTotalScore(Integer totalScore) {
        this.totalScore = totalScore;
    }

    public List<Frame> getFrames() {
        return frames;
    }

    public void setFrames(List<Frame> frames) {
        this.frames = frames;
    }
}
