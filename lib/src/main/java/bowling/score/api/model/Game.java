package bowling.score.api.model;

import java.util.List;

public class Game {
    private List<Player> players;
    private Integer currentFrame;

    public Game(List<Player> players) {
        this.players = players;
        this.currentFrame = 1;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void setPlayers(List<Player> players) {
        this.players = players;
    }

    public Integer getCurrentFrame() {
        return currentFrame;
    }

    public void setCurrentFrame(Integer currentFrame) {
        this.currentFrame = currentFrame;
    }
}
