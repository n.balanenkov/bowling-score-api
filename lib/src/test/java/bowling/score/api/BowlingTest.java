package bowling.score.api;

import bowling.score.api.exception.RuleViolation;
import bowling.score.api.model.Frame;
import bowling.score.api.model.Game;
import bowling.score.api.model.Player;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BowlingTest {
    Bowling bowling = new Bowling();

    @Test
    void createPlayer() {
        Player player1 = bowling.createPlayer("Jack");
        Player player2 = bowling.createPlayer("Bob");
        Player player3 = bowling.createPlayer("Matthew");

        assertEquals("Jack", player1.getName());
        assertEquals(0, player1.getTotalScore());
        assertEquals(0, player1.getFrames().size());

        assertEquals("Bob", player2.getName());
        assertEquals(0, player2.getTotalScore());
        assertEquals(0, player1.getFrames().size());

        assertEquals("Matthew", player3.getName());
        assertEquals(0, player3.getTotalScore());
        assertEquals(0, player1.getFrames().size());
    }

    @Test
    void createGame() {
        Player player1 = bowling.createPlayer("Jack");
        Player player2 = bowling.createPlayer("Bob");
        Player player3 = bowling.createPlayer("Matthew");

        List<Player> players = List.of(player1, player2, player3);

        Game game = bowling.createGame(players);

        assertEquals(1, game.getCurrentFrame());
        assertEquals(players, game.getPlayers());
    }

    @Test
    void addPoints_success() throws Exception {
        Player player1 = bowling.createPlayer("Jack");
        Player player2 = bowling.createPlayer("Bob");
        Player player3 = bowling.createPlayer("Matthew");
        Player player4 = bowling.createPlayer("Ross");

        Game game = bowling.createGame(List.of(player1, player2, player3, player4));

        bowling.addPoints(game, player1, List.of(10));
        bowling.addPoints(game, player2, List.of(7,3)); // 18
        bowling.addPoints(game, player3, List.of(1,2)); // 3
        bowling.addPoints(game, player4, List.of(5,4)); // 9

        bowling.addPoints(game, player1, List.of(10));
        bowling.addPoints(game, player2, List.of(8,2)); // 15
        bowling.addPoints(game, player3, List.of(1,3)); // 4
        bowling.addPoints(game, player4, List.of(4,2)); // 6

        bowling.addPoints(game, player1, List.of(10));
        bowling.addPoints(game, player2, List.of(5,5)); // 14
        bowling.addPoints(game, player3, List.of(1,2)); // 3
        bowling.addPoints(game, player4, List.of(5,4)); // 9

        bowling.addPoints(game, player1, List.of(10));
        bowling.addPoints(game, player2, List.of(4,6)); // 19
        bowling.addPoints(game, player3, List.of(10)); // 13
        bowling.addPoints(game, player4, List.of(6,4)); // 15

        bowling.addPoints(game, player1, List.of(10));
        bowling.addPoints(game, player2, List.of(9,1)); // 11
        bowling.addPoints(game, player3, List.of(1,2)); // 3
        bowling.addPoints(game, player4, List.of(5,5)); // 15

        bowling.addPoints(game, player1, List.of(10));
        bowling.addPoints(game, player2, List.of(1,9)); // 12
        bowling.addPoints(game, player3, List.of(0,5)); // 5
        bowling.addPoints(game, player4, List.of(5,4)); // 9

        bowling.addPoints(game, player1, List.of(10));
        bowling.addPoints(game, player2, List.of(2,8)); // 14
        bowling.addPoints(game, player3, List.of(6,4)); // 20
        bowling.addPoints(game, player4, List.of(10)); // 30

        bowling.addPoints(game, player1, List.of(10));
        bowling.addPoints(game, player2, List.of(4,6)); // 13
        bowling.addPoints(game, player3, List.of(10)); // 20
        bowling.addPoints(game, player4, List.of(10)); // 30

        bowling.addPoints(game, player1, List.of(10));
        bowling.addPoints(game, player2, List.of(3,7)); // 15
        bowling.addPoints(game, player3, List.of(8,2)); // 17
        bowling.addPoints(game, player4, List.of(8,2)); // 20

        bowling.addPoints(game, player1, List.of(10, 10, 10));
        bowling.addPoints(game, player2, List.of(5,5,5)); // 15
        bowling.addPoints(game, player3, List.of(7,3,2)); // 12
        bowling.addPoints(game, player4, List.of(10,10,5)); // 25

        assertEquals(300, player1.getTotalScore());
        assertEquals(146, player2.getTotalScore());
        assertEquals(100, player3.getTotalScore());
        assertEquals(156 , player4.getTotalScore());

    }

    @Test
    void addPoints_exception_shotsAlreadyDone() throws Exception {
        Game game = bowling.createGame(List.of(bowling.createPlayer("Jack"), bowling.createPlayer("Bob")));
        bowling.addPoints(game, game.getPlayers().get(0), List.of(1,2));
        RuleViolation ruleViolation = assertThrows(RuleViolation.class, () -> bowling.addPoints(game, game.getPlayers().get(0), List.of(1,2)));
        assertEquals("You can't add points more than once during one frame.", ruleViolation.getMessage());
    }

    @Test
    void addPoints_exception_notEnoughShots() throws Exception {
        Game game = createGameWithOnePlayer("Jack");

        RuleViolation ruleViolation = assertThrows(RuleViolation.class, () -> bowling.addPoints(game, game.getPlayers().get(0), List.of(1)));
        assertEquals("Please provide a full list of shots for that frame.", ruleViolation.getMessage());

        setGameCurrentFrameToTen(game);

        ruleViolation = assertThrows(RuleViolation.class, () -> bowling.addPoints(game, game.getPlayers().get(0), List.of(10, 1)));
        assertEquals("Please provide a full list of shots for that frame.", ruleViolation.getMessage());

        ruleViolation = assertThrows(RuleViolation.class, () -> bowling.addPoints(game, game.getPlayers().get(0), List.of(7, 3)));
        assertEquals("Please provide a full list of shots for that frame.", ruleViolation.getMessage());
    }

    @Test
    void addPoints_exception_gameIsOver() throws Exception {
        Game game = createGameWithOnePlayer("Jack");
        setGameCurrentFrameToTen(game);
        bowling.addPoints(game, game.getPlayers().get(0), List.of(1, 2));
        RuleViolation ruleViolation = assertThrows(RuleViolation.class, () -> bowling.addPoints(game, game.getPlayers().get(0), List.of(1, 2)));
        assertEquals("The game is over, you can't add any more points.", ruleViolation.getMessage());
    }

    @Test
    void addPoints_exception_notPermittedForThreeShots() {
        Game game = createGameWithOnePlayer("Bob");
        RuleViolation ruleViolation = assertThrows(RuleViolation.class, () -> bowling.addPoints(game, game.getPlayers().get(0), List.of(1, 1, 2)));
        assertEquals("You can't do the third shot unless it's 10th frame and you have scored a strike or spare during first two shots.", ruleViolation.getMessage());
    }

    @Test
    void addPoints_exception_tooManyShots(){
        Game game = createGameWithOnePlayer("Matthew");
        RuleViolation ruleViolation = assertThrows(RuleViolation.class, () -> bowling.addPoints(game, game.getPlayers().get(0), List.of(1,2,3,4,5)));
        assertEquals("You have done too many shots in that frame.", ruleViolation.getMessage());
    }

    @Test
    void addPoints_exception_tooManyPoints() throws Exception {
        Game game = createGameWithOnePlayer("Jack");
        RuleViolation ruleViolation = assertThrows(RuleViolation.class, () -> bowling.addPoints(game, game.getPlayers().get(0), List.of(5, 7)));
        assertEquals("The sum of two shots without a strike can not be bigger than 10.", ruleViolation.getMessage());

        setGameCurrentFrameToTen(game);

        ruleViolation = assertThrows(RuleViolation.class, () -> bowling.addPoints(game, game.getPlayers().get(0), List.of(10,3,8)));
        assertEquals("The sum of two shots without a strike can not be bigger than 10.", ruleViolation.getMessage());
    }

    @Test
    void addPoints_exception_incorrectInput(){
        Game game = createGameWithOnePlayer("Bob");
        RuleViolation ruleViolation = assertThrows(RuleViolation.class, () -> bowling.addPoints(game, game.getPlayers().get(0), List.of(-5, 5)));
        assertEquals("Amount of points should be positive and not bigger than 10.", ruleViolation.getMessage());

        ruleViolation = assertThrows(RuleViolation.class, () -> bowling.addPoints(game, game.getPlayers().get(0), List.of(12, 7)));
        assertEquals("Amount of points should be positive and not bigger than 10.", ruleViolation.getMessage());
    }

    private Game createGameWithOnePlayer(String playerName) {
        return bowling.createGame(List.of(bowling.createPlayer(playerName)));
    }

    private void setGameCurrentFrameToTen(Game game) throws Exception {
        for (int i = 1; i < 10; i++) {
            bowling.addPoints(game, game.getPlayers().get(0), List.of(1, 2));
        }
    }

    @Test
    void getPoints() throws Exception {
        Player player1 = bowling.createPlayer("Jack");
        List<Player> players = List.of(player1);
        Game game = bowling.createGame(players);

        bowling.addPoints(game, player1, List.of(1, 2));
        assertEquals(3, bowling.getPoints(player1));

        bowling.addPoints(game, player1, List.of(10));
        assertEquals(13, bowling.getPoints(player1));

        bowling.addPoints(game, player1, List.of(7, 3));
        assertEquals(33, bowling.getPoints(player1));

        bowling.addPoints(game, player1, List.of(7, 2));
        assertEquals(49, bowling.getPoints(player1));
    }

    @Test
    void getPointsTable() throws Exception {
        Player player1 = bowling.createPlayer("Jack");
        List<Player> players = List.of(player1);
        Game game = bowling.createGame(players);

        bowling.addPoints(game, player1, List.of(1, 2));
        bowling.addPoints(game, player1, List.of(10));
        bowling.addPoints(game, player1, List.of(7, 3));
        bowling.addPoints(game, player1, List.of(7, 2));

        List<Frame> pointsTable = bowling.getPointsTable(player1);

        assertEquals(1, Integer.valueOf(pointsTable.get(0).getFirstThrow()));
        assertEquals(2, Integer.valueOf(pointsTable.get(0).getSecondThrow()));
        assertNull(pointsTable.get(0).getThirdThrow());
        assertEquals(3, pointsTable.get(0).getFrameScore());
        assertEquals(3, pointsTable.get(0).getRunningTotal());

        assertEquals("X", pointsTable.get(1).getFirstThrow());
        assertNull(pointsTable.get(1).getSecondThrow());
        assertNull(pointsTable.get(1).getThirdThrow());
        assertEquals(20, pointsTable.get(1).getFrameScore());
        assertEquals(23, pointsTable.get(1).getRunningTotal());

        assertEquals(7, Integer.valueOf(pointsTable.get(2).getFirstThrow()));
        assertEquals("/", pointsTable.get(2).getSecondThrow());
        assertNull(pointsTable.get(2).getThirdThrow());
        assertEquals(17, pointsTable.get(2).getFrameScore());
        assertEquals(40, pointsTable.get(2).getRunningTotal());

        assertEquals(7, Integer.valueOf(pointsTable.get(3).getFirstThrow()));
        assertEquals(2, Integer.valueOf(pointsTable.get(3).getSecondThrow()));
        assertNull(pointsTable.get(3).getThirdThrow());
        assertEquals(9, pointsTable.get(3).getFrameScore());
        assertEquals(49, pointsTable.get(3).getRunningTotal());
    }


}
